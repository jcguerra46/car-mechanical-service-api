<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;

class CuitValidator implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $digits = array();
        if (strlen($value) != 13) return false;
        for ($i = 0; $i < strlen($value); $i++) {
            if ($i == 2 or $i == 11) {
                if ($value[$i] != '-') return false;
            } else {
                if (!ctype_digit($value[$i])) return false;
                if ($i < 12) {
                    $digits[] = $value[$i];
                }
            }
        }
        $acum = 0;
        foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
            $acum += $digits[$i] * $multiplicador;
        }
        $cmp = 11 - ($acum % 11);
        if ($cmp == 11) $cmp = 0;
        if ($cmp == 10) $cmp = 9;
        return ($value[12] == $cmp);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The CUIT is not valid';
    }

}
