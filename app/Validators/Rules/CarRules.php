<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;
use App\Validators\Rules\Customs\CuitValidator;

class CarRules extends BaseRules {

    /**
     * Rules for creating a car
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'patent' => 'required|string|max:255|unique:cars,patent',
            'brand' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'colour' => 'required|string|max:255',
            'year' => 'required|integer|digits:4'
        ];
    }

    /**
     * Rules for editing a user
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'patent' => 'sometimes|string|max:255|unique:cars,patent',
            'brand' => 'sometimes|string|max:255',
            'model' => 'sometimes|string|max:255',
            'colour' => 'sometimes|string|max:255',
            'year' => 'sometimes|integer|digits:4'
        ];
    }

    /**
     * Rules for show a user
     *
     * @return array
     */
    public static function showRules()
    {
        return [
            //
        ];
    }

    /**
     * Rules for delete a user
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [
            //
        ];
    }
}
