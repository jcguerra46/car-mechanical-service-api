<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;
use App\Validators\Rules\Customs\CuitValidator;

class UserRules extends BaseRules {

    /**
     * Rules for creating a user
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|string|confirmed',
            'phone' => 'required|string|max:20',
            'is_admin' => 'boolean',
            'active' => 'required|boolean',
            'cuit' => [
                'required',
                'string',
                'max:255',
                new CuitValidator(),
            ]
        ];
    }

    /**
     * Rules for editing a user
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'first_name' => 'sometimes|string|max:255',
            'last_name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email|unique:users,email',
            'password' => 'sometimes|min:6|string|confirmed',
            'phone' => 'sometimes|string|max:20',
            'is_admin' => 'sometimes|boolean',
            'active' => 'sometimes|boolean',
            'cuit' => [
                'sometimes',
                'string',
                'max:255',
                new CuitValidator(),
            ]
        ];
    }

    /**
     * Rules for show a user
     *
     * @return array
     */
    public static function showRules()
    {
        return [
            //
        ];
    }

    /**
     * Rules for delete a user
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [
            //
        ];
    }
}
