<?php

namespace App\Http\Controllers\Owner;

use App\Models\Owner;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use App\Validators\Rules\UserRules;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, UserRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(Owner::with('cars')->paginate($pageSize));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $this->validate($request, UserRules::showRules());
        $owner = Owner::findOrFail($id);
        $owner->cars;
        return $this->successResponse($owner);
    }
}
