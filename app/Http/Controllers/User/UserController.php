<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Resources\DestroyResource;
use App\Traits\ApiResponser;
use App\Validators\Rules\UserRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, UserRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(User::with('cars')->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, UserRules::storeRules());
        $user = User::create($request->all());
        return $this->successResponse(
            $user,
            'User created',
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     */
    public function show(Request $request, $id)
    {
            $this->validate($request, UserRules::showRules());
            $user = User::findOrFail($id);
            $user->cars;
            return $this->successResponse($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, UserRules::updateRules());
        $user = User::findOrFail($id);
        $user->update($request->all());
        $user->cars;
        return $this->successResponse(
            $user,
            'User edited',
            Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, UserRules::destroyRules($id));
        $user = User::findOrFail($id);
        $user->cars()->detach();
        $user->delete();
        return $this->successResponse(new DestroyResource($user));
    }
}
