<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Resources\DestroyResource;
use App\Traits\ApiResponser;
use App\Validators\Rules\CarRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CarController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CarRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(Car::with('owners')->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, CarRules::storeRules());
        $car = Car::create($request->all());
        return $this->successResponse(
            $car,
            'Car created',
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $this->validate($request, CarRules::showRules());
        $car = Car::findOrFail($id);
        $car->owners;
        return $this->successResponse($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, CarRules::updateRules());
        $car = Car::findOrFail($id);
        $car->update($request->all());
        $car->owners;
        return $this->successResponse(
            $car,
            'Car edited',
            Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CarRules::destroyRules($id));
        $car = Car::findOrFail($id);
        $car->owners()->detach();
        $car->delete();
        return $this->successResponse(new DestroyResource($car));
    }
}
