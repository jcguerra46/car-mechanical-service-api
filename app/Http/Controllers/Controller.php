<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *     @OA\Info (
 *          title = "Lumen Pickit API APP",
 *          version = "1.0",
 *          @OA\Contact(
 *              email="juancarlosguerra46@gmail.com"
 *          ),
 *     )
 * )
 */
class Controller extends BaseController
{
    //
}
