<?php

namespace App\Models;

class Mechanic extends User
{
    /**
     * Relation hasMany with Service Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
