<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patent',
        'brand',
        'model',
        'colour',
        'year'
    ];

    /**
     * Relation belongsToMany with Owner Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function owners()
    {
        return $this->belongsToMany(Owner::class);
    }

    /**
     * Relation hasMany with Transaction Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
