<?php

namespace App\Models;

use App\Models\Car;
use App\Models\User;

class Owner extends User
{
    /**
     * Relation belongsToMany with movie model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cars()
    {
        return $this->belongsToMany(Car::class,'car_owner')
            ->withPivot('owner_id');
    }
}
