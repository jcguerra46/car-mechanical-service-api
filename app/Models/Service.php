<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    const SERVICE_AVAILABLE = 1;
    const SERVICE_NOT_AVAILABLE = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'price',
        'mechanic_id',
        'status'
    ];

    /**
     * Verify if it is available.
     *
     * @return int
     */
    public function isAvailable()
    {
        return $this->status = self::SERVICE_AVAILABLE;
    }

    /**
     * Relation BelongsTo with Mechanic Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mechanic()
    {
        return $this->belongsTo(Mechanic::class);
    }

    /**
     * Relation HasMany with Transaction Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Transaction::class);
    }
}
