<?php

/**
 * Swagger Annotation
 * @OA\Post(
 *     path="/auth/login",
 *     tags={"Authentication"},
 *     summary="Login User",
 *     description="Login User.",
 *     @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="Email",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Password",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Token retrived.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     )
 * )
 */

/**
 * Swagger Annotation
 * @OA\Post(
 *     path="/auth/logout",
 *     tags={"Authentication"},
 *     summary="Logout User",
 *     description="Logout User.",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Logout retrived.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     )
 * )
 */
