<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/users",
 *     tags={"Users"},
 *     summary="Get user list",
 *     description="Returns list of Users.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="User overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/users",
 *     tags={"Users"},
 *     summary="Create user",
 *     description="Create user.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="Email",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Password",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password_confirmation",
 *         in="query",
 *         description="Password Confirmation",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="phone",
 *         in="query",
 *         description="Phone",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="is_admin",
 *         in="query",
 *         description="Is admin",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=false,
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuit",
 *         in="query",
 *         description="CUIT",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Update Swagger Annotation
 * @OA\Put(
 *     path="/users/{id}",
 *     tags={"Users"},
 *     summary="Update user",
 *     description="Update user.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Car ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="Email",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Password",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="phone",
 *         in="query",
 *         description="Phone",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="is_admin",
 *         in="query",
 *         description="Is admin",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=false,
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuit",
 *         in="query",
 *         description="CUIT",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/users/{id}",
 *     tags={"Users"},
 *     summary="Get a user info",
 *     description="Returns a user info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="User ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="User not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/users/{id}",
 *     tags={"Users"},
 *     summary="Delete a user",
 *     description="Delete user.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="User ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="User not found.",
 *     )
 * )
 */
