<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CarTest extends TestCase
{
    const baseURL = 'cars';

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    'current_page',
                    'data' => [
                        '*' =>
                            [
                                'id',
                                'patent',
                                'brand',
                                'model',
                                'colour',
                                'year',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'links' => [
                        '*' =>
                            [
                                'url',
                                'label',
                                'active'
                            ]
                    ],
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'patent',
                    'brand',
                    'model',
                    'colour',
                    'year',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Store
     * @test
     */
    public function testStore()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'patent' => $faker->unique()->firstName,
            'brand' => $faker->word,
            'model' => $faker->word,
            'colour' => $faker->colorName,
            'year' => $faker->year
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'patent',
                    'brand',
                    'model',
                    'colour',
                    'year',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testUpdate()
    {
        $car = $this->json('GET',
            self::baseURL . '/2',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        $dataUpdateBody = [
            'colour' => $car->response->original['data']['colour'] . ' Updated',
            'year' => $car->response->original['data']['year'] * 1
        ];

        $response = $this->json('PUT',
            self::baseURL . '/2',
            $dataUpdateBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'patent',
                    'brand',
                    'model',
                    'colour',
                    'year',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/2',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            [
                'data' => [
                    'rows_deleted' => [
                        'id',
                        'patent',
                        'brand',
                        'model',
                        'colour',
                        'year',
                        'updated_at',
                        'created_at'
                    ]
                ]
            ]
        );
    }
}
