<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    const baseURL = 'users';

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    'current_page',
                    'data' => [
                        '*' =>
                            [
                                'id',
                                'first_name',
                                'last_name',
                                'email',
                                'cuit',
                                'active',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'links' => [
                        '*' =>
                        [
                            'url',
                            'label',
                            'active'
                        ]
                    ],
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total'
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'cuit',
                    'active',
                    'phone',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Store
     * @test
     */
    public function testStore()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'cuit' => '20-95660815-6',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'password_confirmation' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'active' => true
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'cuit',
                    'active',
                    'phone',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Update
     * @test
     */
    public function testUpdate()
    {
        $user = $this->json('GET',
            self::baseURL . '/2',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        $dataUpdateBody = [
            'first_name' => $user->response->original['data']['first_name'] . ' Updated',
            'last_name' => $user->response->original['data']['last_name'] . ' Updated'
        ];


        $response = $this->json('PUT',
            self::baseURL . '/2',
            $dataUpdateBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'cuit',
                    'active',
                    'phone',
                    'updated_at',
                    'created_at'
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/2',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            [
                'data' => [
                    'rows_deleted' => [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'cuit',
                        'active',
                        'phone',
                        'updated_at',
                        'created_at'
                    ]
                ]
            ]
        );
    }
}
