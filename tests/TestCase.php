<?php

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    static $token;
    static $setUpHasRunOnce = false;

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function setUp(): void
    {
        parent::setUp();

        //Solo se ejecuta una vez
        if (!static::$setUpHasRunOnce) {
            echo(PHP_EOL . 'Ejecutando Migraciones y DatabaseSeeder...' . PHP_EOL);
            Artisan::call('migrate:fresh');
            Artisan::call('db:seed', ['--class' => 'DatabaseSeeder']);
            static::$setUpHasRunOnce = true;

            static::$token = $this->json('post', 'auth/login', [
                'email' => 'john@war.com',
                'password' => '123456',
            ])->response->original['data']['token'];
        }
    }
}
