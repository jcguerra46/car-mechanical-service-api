<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Lumen Pickit API APP v1.0 | ' . $router->app->version();
});

/* Authentication Routes */
$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('/login', 'Auth\AuthController@login');
    $router->post('/logout', ['middleware' => 'auth', 'uses' => 'Auth\AuthController@logout']);
    $router->get('/user', ['middleware' => 'auth', 'uses' => 'Auth\AuthController@getAuthUser']);
});

$router->group(['middleware' => ['auth']], function () use ($router) {

    /** Users Routes */
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get('/', 'User\UserController@index');
        $router->get('/{id:[0-9]+}', 'User\UserController@show');
//        $router->post('/', 'User\UserController@store');
//        $router->put('/{id:[0-9]+}', 'User\UserController@update');
        $router->delete('/{id:[0-9]+}', 'User\UserController@destroy');
    });

    /** Owners Routes */
    $router->group(['prefix' => 'owners'], function () use ($router) {
        $router->get('/', 'Owner\OwnerController@index');
        $router->get('/{id:[0-9]+}', 'Owner\OwnerController@show');
    });

    /** Mechanics Routes */
    $router->group(['prefix' => 'Mechanics'], function () use ($router) {
        $router->get('/', 'Mechanic\MechanicController@index');
        $router->get('/{id:[0-9]+}', 'Mechanic\MechanicController@show');
    });

    /** Cars Routes */
    $router->group(['prefix' => 'cars'], function () use ($router) {
        $router->get('/', 'Car\CarController@index');
        $router->get('/{id:[0-9]+}', 'Car\CarController@show');
        $router->post('/', 'Car\CarController@store');
        $router->put('/{id:[0-9]+}', 'Car\CarController@update');
        $router->delete('/{id:[0-9]+}', 'Car\CarController@destroy');
    });

    /** Transactions Routes */
    $router->group(['prefix' => 'transactions'], function () use ($router) {
        $router->get('/', 'Transaction\TransactionController@index');
        $router->get('/{id:[0-9]+}', 'Transaction\TransactionController@show');
        $router->post('/', 'Transaction\TransactionController@store');
        $router->put('/{id:[0-9]+}', 'Transaction\TransactionController@update');
        $router->delete('/{id:[0-9]+}', 'Transaction\TransactionController@destroy');
    });

    /** Services Routes */
    $router->group(['prefix' => 'services'], function () use ($router) {
        $router->get('/', 'Service\ServiceController@index');
        $router->get('/{id:[0-9]+}', 'Service\ServiceController@show');
    });

});
