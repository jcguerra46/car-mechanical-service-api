<?php

namespace Database\Factories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'patent' => $this->faker->unique()->firstName,
            'brand' => $this->faker->word,
            'model' => $this->faker->word,
            'colour' => $this->faker->colorName,
            'year' => $this->faker->year
        ];
    }
}
