<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'first_name' => 'John',
                    'last_name' => 'War',
                    'email' => 'john@war.com',
                    'password' => Hash::make('123456'),
                    'phone' => '15 2692-4497',
                    'is_admin' => true,
                    'active' => true,
                    'cuit' => '20-95660815-6',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
            ]
        );

        User::factory()
            ->times(49)
            ->create();
    }
}
