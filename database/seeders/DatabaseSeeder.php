<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Car::truncate();
        DB::table('car_owner')->truncate();

        // set user admin
        DB::table('users')->insert(
            [
                [
                    'first_name' => 'John',
                    'last_name' => 'War',
                    'email' => 'john@war.com',
                    'password' => Hash::make('123456'),
                    'phone' => '15 2692-4497',
                    'is_admin' => true,
                    'active' => true,
                    'cuit' => '20-95660815-6',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
            ]
        );

        $usersNumber = 49;
        $carsNumber = 75;

        // users seeder
        User::factory()
            ->times($usersNumber)
            ->create();

        // cars seeder with owners
        Car::factory()
            ->times($carsNumber)
            ->create()->each(
                function($car) {
                    $owners = User::all()
                        ->random(mt_rand(1, 2))
                        ->pluck('id');
                    $car->owners()
                        ->attach($owners);
                }
            );
    }
}
